<?php

function _phptemplate_variables($hook, $vars) {
  $vars = array();
  if ($hook == 'page') {
  	if (module_exists('page_lang')) {
      $vars['language'] = page_lang_page_get_lang();
  	}
  }
  return $vars;
}

********************************************************************
                     D R U P A L    M O D U L E                         
********************************************************************
Name: Page lang module
Version: 0.1
Author: Robert Douglass
Email: rob ad robshouse dot net
Last update: February 21, 2006
Drupal: 4.7

********************************************************************
DESCRIPTION:

 * This module gives you control over the language of page. It gives you the chance
 * to provide templates for how the lang should be structured, and on node
 * pages, gives you the chance to specify the tag xhtml rather than defaulting
 * to the site. This module is the same than page_title, but changes another thinks.

********************************************************************
PERMISSIONS:

This module defines the "set page language" permission. This permission determines
whether a user will se the "Page lang" field on node edit forms.

********************************************************************
SYSTEM REQUIREMENTS:

Drupal: 4.7

********************************************************************
INSTALLATION:

1. Place the entire page_title directory into your Drupal modules/
   directory.
 

2. Enable this module by navigating to:

     administer > modules
     
   At this point the Drupal install system will attempt to create the database
   table page_lang. You should see a message confirming success or
   proclaiming failure. If the database table creation did not succeed,
   you will need to manually add the following table definition to your
   database:
   
    CREATE TABLE `page_lang` (
      `nid` INT NOT NULL ,
      `page_title` VARCHAR( 128 ) NOT NULL ,
      PRIMARY KEY ( `nid` )
    ) TYPE = MYISAM /*!40100 DEFAULT CHARACTER SET utf8 */;    
     
3. Optionally configure the two variations of page language by visiting:
   
    administer > settings > page_lang
    
4. The page language is ultimately set at the theme level. To let your PHPTemplate
   based theme interact with this module, you need to add some code to the template.php
   file that comes with your theme. If there is no template.php file, you can simply
   use the one included with this download. Here is the code:

function _phptemplate_variables($hook, $vars) {
  $vars = array();
  if ($hook == 'page') {
  
    // This is the only important line
    $vars['language'] = page_lang_page_get_lang();
    
  }
  return $vars;
}

  As you can see from the code comment, there is only one important line
  of code:
  
  $vars['language'] = page_lang_page_get_lang();
  
  This line needs to be added to the 'page' hook of the _phptemplate_variables
  function.
  
  Alternately, you can call page_title_page_get_lang() from page.tpl.php
  directly at the place where the lang tag is generated. Normaly is this:
  <html xmlns="http://www.w3.org/1999/xhtml" lang="<?php print $language ?>" xml:lang="<?php print $language ?>">
